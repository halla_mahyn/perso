require 'rails_helper'
RSpec.describe 'Persotag', type: :model, feature: 'models/persotag' do

  describe 'Validation' do

    before do
      @persotag = Persotag.new(label: 'V1')
    end

    it 'with valid characters' do
      @persotag.description = 'так называемый <persotag>1'
      expect(@persotag.valid?).to eq(true)
    end

    it 'with non valid characters' do
      @persotag.description = 'это описание не валидно(%, sorry'
      expect(@persotag.valid?).to eq(false)
    end

    it 'description must not exceed 30 characters' do
      @persotag.description = 'эта строка равна 29и символам'
      expect(@persotag.valid?).to eq(true)
    end

    it 'this description exceed 30 characters' do
      @persotag.description = 'а эта строка уже больше допустимой длины'
      expect(@persotag.valid?).to eq(false)
    end

  end
end
