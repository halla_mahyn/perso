class PersotagsController < ApplicationController

  def index
    @persotags = Persotag.all
  end

  def create
    persotag = Persotag.new(persotags_params)
    if persotag.save
      render json: persotag, status: 201
    else
      render json: persotag.errors.full_messages, status: 422
    end
  end

  def delete
  end

  def edit
  end

  
  private 

  def persotags_params
    params.require(:persotag).permit(:label, :description)
  end
end
