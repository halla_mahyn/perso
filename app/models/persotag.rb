class Persotag < ApplicationRecord
  validates :description, length: { maximum: 30,
                                    message: 'Descriptions size must be lower or equal 30 characters'
                                  }
  validates :description, format: { without: /[^a-zA-Z\/ \/<>0-9а-яА-Я]+/,
                                    message: "Allowed only: 'a-z', 'а-я', whitspace and '<>' characters"
                                  } 
end
